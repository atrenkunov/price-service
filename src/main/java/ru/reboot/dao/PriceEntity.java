package ru.reboot.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "price")
public class PriceEntity {

    @Id
    @Column(name = "item_id")
    private String itemId;

    @Column(name = "price")
    private Double price;

    @Column(name = "last_access_time")
    private LocalDateTime lastAccessTime;
}

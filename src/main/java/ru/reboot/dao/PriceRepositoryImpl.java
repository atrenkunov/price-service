package ru.reboot.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

/**
 * Price repository.
 */
@Component
public class PriceRepositoryImpl implements PriceRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PriceEntity findPrice(String itemId) {
        return null;
    }

    @Override
    public List<PriceEntity> findAllPrices() {
        return null;
    }

    @Override
    public PriceEntity savePrice(PriceEntity price) {
        return null;
    }

    @Override
    public Collection<PriceEntity> saveAllPrices(Collection<PriceEntity> prices) {
        return null;
    }

    @Override
    public void deletePrice(String itemId) {
    }
}

package ru.reboot.dao;

import java.util.Collection;
import java.util.List;

public interface PriceRepository {

    /**
     * Find item price
     */
    PriceEntity findPrice(String itemId);

    /**
     * Find all prices
     */
    List<PriceEntity> findAllPrices();

    /**
     * Set item price
     */
    PriceEntity savePrice(PriceEntity price);

    /**
     * Set all price
     */
    Collection<PriceEntity> saveAllPrices(Collection<PriceEntity> prices);

    /**
     * Delete price
     */
    void deletePrice(String itemId);
}

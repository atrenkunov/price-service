package ru.reboot.controller;

import ru.reboot.dto.PriceInfo;

import java.util.Collection;
import java.util.List;

public interface PriceController {

    /**
     * Get item price
     */
    PriceInfo getPrice(String itemId);

    /**
     * Find all prices
     */
    List<PriceInfo> findAllPrices();

    /**
     * Set item price
     */
    PriceInfo savePrice(PriceInfo price);

    /**
     * Set all price
     */
    Collection<PriceInfo> saveAllPrices(Collection<PriceInfo> prices);

    /**
     * Delete price
     */
    void deletePrice(String itemId);
}

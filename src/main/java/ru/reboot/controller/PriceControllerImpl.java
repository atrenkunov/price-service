package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dao.PriceRepositoryImpl;
import ru.reboot.dto.PriceInfo;
import ru.reboot.service.PriceService;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Price controller.
 */
@RestController
@RequestMapping(path = "price")
public class PriceControllerImpl implements PriceController {

    private static final Logger logger = LogManager.getLogger(PriceRepositoryImpl.class);

    private PriceService priceService;

    @Autowired
    public void setPriceService(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "PriceController " + new Date();
    }

    @Override
    public PriceInfo getPrice(String itemId) {
        return null;
    }

    @Override
    public List<PriceInfo> findAllPrices() {
        return null;
    }

    @Override
    public PriceInfo savePrice(PriceInfo price) {
        return null;
    }

    @Override
    public Collection<PriceInfo> saveAllPrices(Collection<PriceInfo> prices) {
        return null;
    }

    @Override
    public void deletePrice(String itemId) {
    }
}

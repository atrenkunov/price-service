package ru.reboot.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.reboot.dao.PriceRepository;
import ru.reboot.dto.PriceInfo;
import ru.reboot.error.BusinessLogicException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Component
public class PriceServiceImpl implements PriceService {

    private PriceRepository priceRepository;

    @Autowired
    public void setPriceRepository(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    @Override
    public PriceInfo getPrice(String itemId) {
        return null;
    }

    @Override
    public List<PriceInfo> findAllPrices() {
        return null;
    }

    @Override
    public PriceInfo savePrice(PriceInfo price) {
        return null;
    }

    @Override
    public Collection<PriceInfo> saveAllPrices(Collection<PriceInfo> prices) {
        return null;
    }

    @Override
    public void deletePrice(String itemId) {
    }
}

package ru.reboot.dto;

import java.time.LocalDateTime;

public class PriceInfo {

    private String itemId;
    private Double price;
    private LocalDateTime lastAccessTime;
}

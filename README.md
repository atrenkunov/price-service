# Сервис price-service

Данный сервис отвечает за ценообразование товаров.

Ответственный за сервис: Светлана Лапина

## План работ:

#### Создать таблицу price. Написать liquibase скрипт. Данная таблица хранит информацию по ценам.

Поля таблицы priceInfo

  * item_id: string
  * price: double
  
  // дата-время, когда данная цена была установлена (дата добавления в БД)
  * last_access_time: LocalDateTime

### Создать соответствующий класс PriceInfo.

### Реализовать rest-контроллер /price/**/*

План работы:

Светлана Лапина
- Написать ликубейс скрипты и разместить их в папке sql, написать структуру PriceInfo, PriceEntity, getters & setters
- Получить информацию о цене на определенный товар GET /price?itemId={itemId}

Никита Кителев
- Удалить информацию о цене за товар DELETE /price?itemId={itemId}

Кремнева Наталья
- Установить цену на список товаров PUT /price/all

Евгений Деревянка
- Получить все цены GET /price/all

Максим Ефремов
- Установить цену на товар PUT /price

Критерии приемки:
- методы сервиса должны быть протестированы.